/*

  Copyright 2017 Loopring Project Ltd (Loopring Foundation).

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

*/

package manager

import (
	"github.com/Loopring/relay-lib/cache"
	"github.com/Loopring/relay-lib/eventemitter"
	"github.com/Loopring/relay-lib/log"
	"github.com/Loopring/relay-lib/types"
	"math/big"
	"strconv"
	"strings"
	"time"
)

const DefaultP2POrderExpireTime = 3600 * 24 * 7
const p2pOrderPreKey = "P2P_OWNER_"
const p2pRelationPreKey = "P2P_RELATION_"
const p2pTakerPreKey = "P2P_TAKER_"
const splitMark = "_"

func init() {
	p2pRingMinedWatcher := &eventemitter.Watcher{Concurrent: false, Handle: HandleP2PRingMined}
	eventemitter.On(eventemitter.OrderFilled, p2pRingMinedWatcher)
}

func SaveP2POrderRelation(takerOwner, taker, makerOwner, maker, txHash, pendingAmount, validUntil string) error {

	takerOwner = strings.ToLower(takerOwner)
	taker = strings.ToLower(taker)
	makerOwner = strings.ToLower(makerOwner)
	maker = strings.ToLower(maker)
	txHash = strings.ToLower(txHash)

	cache.SAdd(p2pOrderPreKey+takerOwner, DefaultP2POrderExpireTime, []byte(taker))
	cache.SAdd(p2pOrderPreKey+makerOwner, DefaultP2POrderExpireTime, []byte(maker))
	cache.Set(p2pRelationPreKey+taker, []byte(txHash), DefaultP2POrderExpireTime)
	cache.Set(p2pRelationPreKey+maker, []byte(txHash), DefaultP2POrderExpireTime)

	log.Info("p2porder redis store failed111")
	untilTime, _ := strconv.ParseInt(validUntil, 10, 64)
	nowTime := time.Now().UnixNano() / int64(1000000000)
	takerExpiredTime := untilTime - nowTime
	log.Info("p2porder redis store failed222")
	err := cache.ZAdd(p2pTakerPreKey+maker, takerExpiredTime, []byte(strconv.FormatInt(nowTime, 10)), []byte(txHash+splitMark+pendingAmount))
	log.Info("p2porder redis store failed333")
	if err != nil {
		log.Info("p2porder redis store failed" + err.Error())
	}

	return nil
}

func IsP2PMakerLocked(maker string) bool {
	exist, err := cache.Exists(p2pRelationPreKey + maker)
	if err != nil || exist == true {
		return true
	}
	return false
}

func HandleP2PRingMined(input eventemitter.EventData) error {
	if evt, ok := input.(*types.OrderFilledEvent); ok && evt != nil && evt.Status == types.TX_STATUS_SUCCESS {
		cache.SRem(p2pOrderPreKey+strings.ToLower(evt.Owner.Hex()), []byte(strings.ToLower(evt.OrderHash.Hex())))
		cache.Del(p2pRelationPreKey + strings.ToLower(evt.OrderHash.Hex()))
		cache.Del(p2pRelationPreKey + strings.ToLower(evt.NextOrderHash.Hex()))
	}
	return nil
}

func GetP2PPendingAmount(maker string) (pendingAmount *big.Rat, err error) {
	pendingAmount = new(big.Rat)
	maker = strings.ToLower(maker)
	nowTime := time.Now().UnixNano() / int64(1000000000)
	log.Info("p2porder submit for p2p start 91...")
	if data, err := cache.ZRange(p2pTakerPreKey+maker, int64(0), nowTime, false); nil != err {
		log.Info("p2porder submit for p2p start 93..." + err.Error())
		return pendingAmount, err
	} else {
		for _, v := range data {
			log.Info("p2porder submit for p2p start 97..." + string(v))
			pendData, _ := new(big.Int).SetString(strings.Split(string(v), splitMark)[1], 0)
			pendingAmount = pendingAmount.Add(pendingAmount, new(big.Rat).SetInt(pendData))
			log.Info("p2porder submit for p2p start 100..." + pendingAmount.String())
		}
		return pendingAmount, nil
	}

}
